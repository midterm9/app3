package com.natruja.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val arrayAdapter: ArrayAdapter<*>
        val gas= arrayOf(
            "4.84",
            "35.68",
            "35.95",
            "43.44",
            "34.94",
            "34.94",
            "34.94",
            "34.94",
            "45.66"
        )

        var mListView = findViewById<ListView>(R.id.userlist)
        arrayAdapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1, gas)
        mListView.adapter = arrayAdapter
    }
}